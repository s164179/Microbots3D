#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <vector>

#include "Bot.h"

class Simulator{
    public:
        std::vector<Bot> bots, destinations;
        std::vector<std::vector<Bot> > path;
        int timeSlice;
        bool dir;
        Simulator();
        bool update();
        void draw(bool showEndPoints, bool showPath, bool showInterpolation);
        void reset();
        void correct(std::vector<Bot>& bots);
    private:
        bool showPaths, showEndPoints;
        float collision(std::vector<Bot>& bots);
        float resolveConnections(std::vector<Bot>& bots);
        void minimumTree(std::vector<Bot>& bots);
        std::vector<Bot> interpolate(const std::vector<Bot>& bots, const std::vector<Bot>& destinations, float w);
        std::vector<Bot> interpolate(const std::vector<Bot>& b1, const std::vector<Bot>& b2, const std::vector<Bot>& b3, const std::vector<Bot>& b4);
        float maximumDistance(const std::vector<Bot>& bots, const std::vector<Bot>& destinations);
        float squareSumDistance(const std::vector<Bot>& bots, const std::vector<Bot>& destinations);
        float squareSumAcceleration(const std::vector<Bot>& b1, const std::vector<Bot>& b2, const std::vector<Bot>& b3);
        void assignmentProblem(const std::vector<Bot>& bots, std::vector<Bot>& destinations);
};

#endif // SIMULATOR_H
