#ifndef VEC2_H
#define VEC2_H

#include <ostream>

struct vec2{
    double x, y;
    vec2();
    vec2(double x, double y);
};

vec2 operator-(const vec2& v);
vec2 operator+(const vec2& v1, const vec2& v2);
vec2 operator-(const vec2& v1, const vec2& v2);
vec2 operator*(const vec2& v, double n);
vec2 operator*(double n, const vec2& v);
vec2 operator/(const vec2& v, double n);
void operator+=(vec2& v1, const vec2& v2);
void operator-=(vec2& v1, const vec2& v2);
void operator*=(vec2& v, double n);
void operator/=(vec2& v, double n);
double operator*(const vec2& v1, const vec2& v2);
std::ostream& operator<<(std::ostream& os, const vec2& v);

#endif // vec2_H
