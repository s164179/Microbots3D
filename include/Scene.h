#ifndef SCENE_H
#define SCENE_H

#undef __STRICT_ANSI__
#include <math.h>
#define SFML_STATIC
#include <SFML/Graphics.hpp>
#include <vector>

#include "vec2.h"
#include "vec3.h"
#include "Simulator.h"

class Scene{
    public:
        Scene(sf::RenderWindow& window);
        bool input(sf::Event& event);
        bool inputMousePos(const vec2& pos);
        bool update();
        void draw();
    private:
        sf::RenderWindow& window;
        float aspectRatio, fov;
        bool keyLeft, keyRight, keyUp, keyDown, keySpace, keyCtrl, keyShift, keyAlt, keyF2, lmb, rmb, mmb;
        vec2 mouse, mousePressed;
        vec3 camPos, camZoom, cursor;
        Simulator simulator;
        bool showPaths, showEndPoints, showInterpolation, showCursor;
        std::string getFilePath(bool load);
        std::string getFilePathSave();
        void save(const std::string& path);
        void load(const std::string& path, std::vector<Bot>& bots);
        bool insertBot(std::vector<Bot>& bots, const vec3& pos);
        void insertBox(std::vector<Bot>& bots, const vec3& pos, const vec3& s, bool hollow);
};

#endif // SCENE_H
