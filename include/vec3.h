#ifndef VEC3_H
#define VEC3_H

#include <ostream>
#include <istream>

struct vec3{
    double x, y, z;
    vec3();
    vec3(double x, double y, double z);
};

bool operator==(const vec3& v1, const vec3& v2);
bool operator!=(const vec3& v1, const vec3& v2);
vec3 operator-(const vec3& v);
vec3 operator+(const vec3& v1, const vec3& v2);
vec3 operator-(const vec3& v1, const vec3& v2);
vec3 operator*(const vec3& v, double n);
vec3 operator*(double n, const vec3& v);
vec3 operator/(const vec3& v, double n);
void operator+=(vec3& v1, const vec3& v2);
void operator-=(vec3& v1, const vec3& v2);
void operator*=(vec3& v, double n);
void operator/=(vec3& v, double n);
double operator*(const vec3& v1, const vec3& v2);
std::ostream& operator<<(std::ostream& os, const vec3& v);
std::istream& operator>>(std::istream& is, vec3& v);

#endif // VEC3_H
