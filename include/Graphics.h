#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "vec3.h"

namespace Graphics{
    namespace{
        vec3 col;
    }
    void color(const vec3& c);
    void line(const vec3& a, const vec3& b);
    void grid(const vec3& pos, const vec3& v1, const vec3& v2, int w, int h);
    void sphere(const vec3& pos, float r);
};

#endif // GRAPHICS_H
