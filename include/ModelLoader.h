#ifndef MODELLOADER_H
#define MODELLOADER_H

#include <vector>

#include <GEL/CGLA/Mat4x4d.h>

#include <GEL/Geometry/RGrid.h>
#include <GEL/Geometry/GridAlgorithm.h>
#include <GEL/Geometry/build_bbtree.h>
#include <GEL/Geometry/AABox.h>

#include <GEL/HMesh/triangulate.h>
#include <GEL/HMesh/Manifold.h>

#include <GEL/HMesh/load.h>

#include "vec3.h"

class ModelLoader{
    public:
        std::vector<vec3> loadModel(const std::string& path, int s);
    private:
        std::pair<std::vector<std::vector<std::vector<float> > >, vec3> loadDistanceField(const std::string& path, int s);

        const CGLA::Mat4x4d fit_bounding_volume(const CGLA::Vec3d& p0, const CGLA::Vec3d& p7, float buf_reg, const CGLA::Vec3i& vol_dim);

        template<class BBTree>
        class DistCompCache{
            BBTree *T;
            mutable float old_d;
            mutable CGLA::Vec3i old_p;
        public:

            DistCompCache(BBTree* _T): T(_T), old_p(-99999) {}

            void operator()(const CGLA::Vec3i& pi, float& vox_val) const
            {
                CGLA::Vec3f p(pi);
                if(sqr_length(pi-old_p)==1)
                    vox_val = T->compute_signed_distance(p,CGLA::sqr(1.001+fabs(old_d)));
                else
                    vox_val = T->compute_signed_distance(p);
                old_p = pi;
                old_d = vox_val;
            }
        };
};

#endif // MODELLOADER_H
