#ifndef BOT_H
#define BOT_H

#include <vector>

#include "vec3.h"

class Bot{
    public:
        static float r;
        vec3 pos;
        std::vector<int> connections;
        bool ground;
        Bot();
        Bot(const vec3& pos);
    private:
};

#endif // BOT_H
