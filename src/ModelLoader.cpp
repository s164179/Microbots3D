#include "ModelLoader.h"

#include <iostream>

#include <GEL/CGLA/Mat4x4d.h>

#include <GEL/Geometry/RGrid.h>
#include <GEL/Geometry/GridAlgorithm.h>
#include <GEL/Geometry/build_bbtree.h>
#include <GEL/Geometry/AABox.h>

#include <GEL/HMesh/triangulate.h>
#include <GEL/HMesh/Manifold.h>

#include <GEL/HMesh/load.h>

#include "Bot.h"

using namespace std;
using namespace HMesh;
using namespace Geometry;
using namespace CGLA;

const Mat4x4d ModelLoader::fit_bounding_volume(const Vec3d& p0, const Vec3d& p7, float buf_reg, const Vec3i& vol_dim){
    Vec3d sz = p7-p0;
    Vec3i dims = vol_dim;
    Vec3d scal_vec = (Vec3d(dims)-Vec3d(2*buf_reg+2))/sz;
    float scal = min(scal_vec[0], min(scal_vec[1], scal_vec[2]));

    Mat4x4d m = translation_Mat4x4d(Vec3d(0)+Vec3d(buf_reg+1));
    m *= scaling_Mat4x4d(Vec3d(scal));
    m *= translation_Mat4x4d(-p0);
    return m;
}

vector<vec3> ModelLoader::loadModel(const string& path, int s){
    vector<vec3> result;

    vector<vector<vector<float> > > grid = loadDistanceField(path, s).first;

    for(int i = 0; i < s; ++i){
        for(int j = 0; j < s; ++j){
            for(int k = 0; k < s; ++k){
                if(grid[i][j][k] < 0) result.push_back(vec3(i, j, k)*2*Bot::r);
            }
        }
    }
    if(result.empty()) return result;
    vec3 a = result[0], b = result[0];
    for(int i = 0; i < result.size(); ++i){
        a.x = fmin(a.x, result[i].x);
        a.y = fmin(a.y, result[i].y);
        a.z = fmin(a.z, result[i].z);
        b.x = fmax(b.x, result[i].x);
        b.y = fmax(b.y, result[i].y);
        b.z = fmax(b.z, result[i].z);
    }
    vec3 c = (a+b)/2;
    for(int i = 0; i < result.size(); ++i){
        result[i] -= c;
        result[i].y += c.y-a.y+Bot::r;
    }
    return result;
}

pair<vector<vector<vector<float> > >, vec3> ModelLoader::loadDistanceField(const string& path, int s){
    Vec3i vol_dim(s);
    Manifold m;
    if(!load(path, m)){
        cout << "Failed to load mesh" << endl;
        exit(1);
    }
    string file_prefix = path.substr(0, path.length()-4) + "-";
	cout << "Volume dimensions " << vol_dim << endl;
	if(!valid(m)){
		cout << "Not a valid manifold" << endl;
		exit(0);
	}
    triangulate(m);

	Vec3d p0, p7;
	bbox(m, p0, p7);

	Mat4x4d T = fit_bounding_volume(p0,p7,0,vol_dim);

    cout << "Transformation " << T << endl;

	for(VertexIDIterator v = m.vertices_begin(); v != m.vertices_end(); ++v)
		m.pos(*v) = T.mul_3D_point(m.pos(*v));

 	RGrid<float> grid(vol_dim,FLT_MAX);

    cout << "Building AABB Tree" << endl;
    AABBTree aabb_tree;
    build_AABBTree(m, aabb_tree);

    cout << "Computing distances from AABB Tree" << endl;
    DistCompCache<AABBTree> dist(&aabb_tree);
    for_each_voxel(grid, dist);

    vector<vector<vector<float> > > result(s, vector<vector<float> >(s, vector<float>(s, 0)));

    for(int i = 0; i < s; ++i){
        for(int j = 0; j < s; ++j){
            for(int k = 0; k < s; ++k){
                result[i][j][k] = grid[Vec3i(i,j,k)];
            }
        }
    }

    vec3 a(s, s, s), b(0, 0, 0);
    for(int i = 0; i < s; ++i){
        for(int j = 0; j < s; ++j){
            for(int k = 0; k < s; ++k){
                if(result[i][j][k] > 0) continue;
                a.x = fmin(a.x, i);
                a.y = fmin(a.y, j);
                a.z = fmin(a.z, k);
                b.x = fmax(b.x, i);
                b.y = fmax(b.y, j);
                b.z = fmax(b.z, k);
            }
        }
    }
    vec3 c = (a+b)/2;
    vec3 d = b-a;

    return make_pair(result, -c+vec3(0, d.y/2+Bot::r, 0));
}
