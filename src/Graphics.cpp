#include "Graphics.h"

#undef __STRICT_ANSI__
#include <math.h>
#define SFML_STATIC
#include <SFML/Graphics.hpp>
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <vector>

using namespace std;

void Graphics::color(const vec3& c){
    col = c;
    glColor3f(col.x, col.y, col.z);
}

void Graphics::line(const vec3& a, const vec3& b){
    glBegin(GL_LINES);
    glVertex3f(a.x, a.y, a.z);
    glVertex3f(b.x, b.y, b.z);
    glEnd();
}

void Graphics::grid(const vec3& pos, const vec3& v1, const vec3& v2, int w, int h){
    glBegin(GL_LINES);
    for(int i = -w; i <= w; ++i){
        vec3 u1 = i*v1-h*v2;
        vec3 u2 = i*v1+h*v2;
        glVertex3f(u1.x, u1.y, u1.z);
        glVertex3f(u2.x, u2.y, u2.z);
    }
    for(int i = -h; i <= h; ++i){
        vec3 u1 = i*v2-w*v1;
        vec3 u2 = i*v2+w*v1;
        glVertex3f(u1.x, u1.y, u1.z);
        glVertex3f(u2.x, u2.y, u2.z);
    }
    glEnd();
}

void Graphics::sphere(const vec3& pos, float r){
    static vector<float> vertices, colors;
    static vec3 colorPrev;
    if(vertices.empty() || colorPrev != col){
        colorPrev = col;
        vertices.clear();
        colors.clear();
        int precision = 8;
        for(int i = 0; i < 2*precision; ++i){
            float u1 = 2*M_PI*i/precision;
            float u2 = 2*M_PI*(i+1)/precision;
            for(int j = 0; j < precision; ++j){
                float v1 = M_PI*j/precision-M_PI_2;
                float v2 = M_PI*(j+1)/precision-M_PI_2;
                vec3 col1 = col*(0.5*sin(v1)+0.5);
                vec3 col2 = col*(0.5*sin(v2)+0.5);

                vertices.push_back(cos(u1)*cos(v1));
                vertices.push_back(sin(v1));
                vertices.push_back(sin(u1)*cos(v1));

                vertices.push_back(cos(u2)*cos(v1));
                vertices.push_back(sin(v1));
                vertices.push_back(sin(u2)*cos(v1));

                vertices.push_back(cos(u2)*cos(v2));
                vertices.push_back(sin(v2));
                vertices.push_back(sin(u2)*cos(v2));

                vertices.push_back(cos(u2)*cos(v2));
                vertices.push_back(sin(v2));
                vertices.push_back(sin(u2)*cos(v2));

                vertices.push_back(cos(u1)*cos(v2));
                vertices.push_back(sin(v2));
                vertices.push_back(sin(u1)*cos(v2));

                vertices.push_back(cos(u1)*cos(v1));
                vertices.push_back(sin(v1));
                vertices.push_back(sin(u1)*cos(v1));

                colors.push_back(col1.x);
                colors.push_back(col1.y);
                colors.push_back(col1.z);

                colors.push_back(col1.x);
                colors.push_back(col1.y);
                colors.push_back(col1.z);

                colors.push_back(col2.x);
                colors.push_back(col2.y);
                colors.push_back(col2.z);

                colors.push_back(col2.x);
                colors.push_back(col2.y);
                colors.push_back(col2.z);

                colors.push_back(col2.x);
                colors.push_back(col2.y);
                colors.push_back(col2.z);

                colors.push_back(col1.x);
                colors.push_back(col1.y);
                colors.push_back(col1.z);
            }
        }
    }
    glPushMatrix();
    glTranslatef(pos.x, pos.y, pos.z);
    glScalef(r, r, r);
    glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
    glColorPointer(3, GL_FLOAT, 0, &colors[0]);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size()/3);
    glPopMatrix();
}
