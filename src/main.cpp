#define SFML_STATIC
#include <SFML/Graphics.hpp>
#define GLEW_STATIC
#include <GL/glew.h>
#include <iostream>

#include "Scene.h"

using namespace std;

int main(int argc, char* argv[]){
    sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "Microbots", sf::Style::Fullscreen, sf::ContextSettings(24,8,8));
    window.setVerticalSyncEnabled(true);
    glewInit();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glClearColor(0,0,0,1);
    Scene scene(window);
    sf::Event event;
    sf::Clock cl;
    int c = 0;
    while(true){
        ++c;
        if(c == 60){
            static float fpsTime = cl.getElapsedTime().asSeconds();
            float t = cl.getElapsedTime().asSeconds();
            float fps = c/(t-fpsTime);
            fpsTime = t;
            c = 0;
            cout << "fps: " << fps << endl;
        }
        bool inp = true;
        while(window.pollEvent(event)){
            if(!scene.input(event)){
                inp = false;
                break;
            }
        }
        if(!inp) break;
        sf::Vector2i m = sf::Mouse::getPosition(window);
        if(!scene.inputMousePos(vec2((float)m.x/window.getSize().y,1-(float)m.y/window.getSize().y))) break;
        if(!scene.update()) break;
        scene.draw();
    }
    window.close();
    return 0;
}
