#include "Scene.h"

#undef __STRICT_ANSI__
#include <math.h>
#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <windows.h>

#include "Graphics.h"
#include "ModelLoader.h"

using namespace std;

Scene::Scene(sf::RenderWindow& window):
        window(window),
        fov(M_PI_2),
        camPos(0, 0, 0), camZoom(0, M_PI_4, 5), cursor(0, Bot::r, 0){
    keyUp = keyDown = keyLeft = keyRight = keySpace = keyCtrl = keyShift = keyAlt = keyF2 = lmb = rmb = mmb = false;
    showPaths = showEndPoints = showInterpolation = showCursor = true;
}

bool Scene::input(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            return false;
        case sf::Event::KeyPressed:
        case sf::Event::KeyReleased:{
            bool down = event.type == sf::Event::KeyPressed;
            switch(event.key.code){
                case sf::Keyboard::Escape:
                    if(down) return false;
                    break;
                case sf::Keyboard::Up:
                    keyUp = down;
                    break;
                case sf::Keyboard::Down:
                    keyDown = down;
                    break;
                case sf::Keyboard::Left:
                    keyLeft = down;
                    break;
                case sf::Keyboard::Right:
                    keyRight = down;
                    break;
                case sf::Keyboard::Space:
                    keySpace = down;
                    if(down) simulator.update();
                    break;
                case sf::Keyboard::Return:
                    break;
                case sf::Keyboard::LControl:
                case sf::Keyboard::RControl:
                    keyCtrl = down;
                    break;
                case sf::Keyboard::LShift:
                case sf::Keyboard::RShift:
                    keyShift = down;
                    break;
                case sf::Keyboard::LAlt:
                case sf::Keyboard::RAlt:
                    keyAlt = down;
                    break;
                case sf::Keyboard::P:
                    if(down) showPaths = !showPaths;
                    break;
                case sf::Keyboard::E:
                    if(down) showEndPoints = !showEndPoints;
                    break;
                case sf::Keyboard::I:
                    if(down) showInterpolation = !showInterpolation;
                    break;
                case sf::Keyboard::C:
                    if(down) showCursor = !showCursor;
                    break;
                case sf::Keyboard::R:
                    if(down) simulator.reset();
                    break;
                case sf::Keyboard::Q: // clear path
                    if(down) simulator.path.clear();
                    break;
                case sf::Keyboard::A: // reset/shuffle assignment
                    if(!down) break;
                    simulator.path.resize(2);
                    simulator.path[0] = simulator.bots;
                    simulator.path[1] = simulator.destinations;
                    if(keyCtrl) random_shuffle(simulator.path[0].begin(), simulator.path[0].end());
                    if(keyShift){
                        random_shuffle(simulator.bots.begin(), simulator.bots.end());
                        simulator.path.clear();
                    }
                    break;
                case sf::Keyboard::Num0:
                    if(!down) break;
                    camPos = vec3(0, 0, 0);
                    camZoom = vec3(0, M_PI_4, 5);
                    break;
                case sf::Keyboard::S:{
                    if(!keyCtrl) break;
                    string path = getFilePath(false);
                    if(path != "") save(path);
                    break;}
                case sf::Keyboard::O:{
                    if(!keyCtrl) break;
                    string path = getFilePath(true);
                    if(path != "") load(path, keyShift ? simulator.destinations : simulator.bots);
                    break;}
                case sf::Keyboard::X:
                    if(!down) break;
                    camZoom.x = round(camZoom.x*4/M_PI+1)*M_PI/4;
                    if(camZoom.x > M_PI) camZoom.x = -M_PI*3/4;
                    break;
                case sf::Keyboard::Z:
                    if(!down) break;
                    camZoom.y = round(camZoom.y*4/M_PI+1)*M_PI/4;
                    if(camZoom.y > M_PI_2) camZoom.y = -M_PI_2;
                    break;
                case sf::Keyboard::PageUp:
                    if(!down) break;
                    camPos.y += Bot::r*2;
                    break;
                case sf::Keyboard::PageDown:
                    if(!down) break;
                    camPos.y -= Bot::r*2;
                    break;
                case sf::Keyboard::F2:
                    keyF2 = down;
                    break;
                case sf::Keyboard::F11:
                    if(!down) break;
                    static bool full = true;
                    full = !full;
                    window.create(full ? sf::VideoMode::getDesktopMode() : sf::VideoMode(640, 480), "Microbots", full ? sf::Style::Fullscreen : sf::Style::Default, sf::ContextSettings(24,8,8));
                    window.setVerticalSyncEnabled(true);
                    glEnable(GL_BLEND);
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    glEnable(GL_DEPTH_TEST);
                    glEnable(GL_MULTISAMPLE);
                    glEnableClientState(GL_VERTEX_ARRAY);
                    glEnableClientState(GL_COLOR_ARRAY);
                    glClearColor(0,0,0,1);
                    break;
                default:
                    break;
            }
            break;
        }
        case sf::Event::MouseButtonPressed:
        case sf::Event::MouseButtonReleased:{
            bool down = event.type == sf::Event::MouseButtonPressed;
            switch(event.mouseButton.button){
                case sf::Mouse::Left:{
                    lmb = down;
                    if(!down) break;
                    break;}
                case sf::Mouse::Right:
                    rmb = down;
                    if(!down) break;
                    break;
                case sf::Mouse::Middle:
                    mmb = down;
                    if(!down) break;
                    mousePressed = mouse;
                    break;
                default:
                    break;
            }
            bool l = event.mouseButton.button == sf::Mouse::Left;
            bool r = event.mouseButton.button == sf::Mouse::Right;
            if(!down) break;
            if(!l && !r) break;
            vector<Bot>& bots = (l ? simulator.bots : simulator.destinations);
            insertBot(bots, cursor);
            break;
        }
        case sf::Event::MouseWheelMoved:
            if(keyCtrl) camZoom.z *= pow(1.1, -event.mouseWheel.delta);
            else{
                cursor.y -= event.mouseWheel.delta*2*Bot::r;
                if(cursor.y < 0) cursor.y = Bot::r;
            }
            break;
        default:
            break;
    }
    return true;
}

bool Scene::inputMousePos(const vec2& pos){
    mouse = pos;
    vec3 eye = camPos+vec3(cos(camZoom.x)*cos(camZoom.y), sin(camZoom.y), -sin(camZoom.x)*cos(camZoom.y))*camZoom.z;
    vec3 m = vec3(0, mouse.y-0.5, -(mouse.x-aspectRatio/2))*2*tan(fov/2);
    m.x = -1;
    m = vec3(cos(camZoom.y)*m.x-sin(camZoom.y)*m.y, sin(camZoom.y)*m.x+cos(camZoom.y)*m.y, m.z);
    m = vec3(cos(camZoom.x)*m.x+sin(camZoom.x)*m.z, m.y, -sin(camZoom.x)*m.x+cos(camZoom.x)*m.z);
    m *= fabs(eye.y/m.y);
    float y = cursor.y;
    cursor = eye+m;
    cursor = vec3(round(cursor.x-0.5)+0.5, 0.5, round(cursor.z-0.5)+0.5);
    cursor.y = y;
    return true;
}

bool Scene::update(){
    if(mmb){
        vec2 d = mouse-mousePressed;
        vec2 mp = mousePressed*window.getSize().y;
        sf::Mouse::setPosition(sf::Vector2i(mp.x, window.getSize().y-mp.y), window);
        camZoom.x -= d.x*5;
        if(keyCtrl) camZoom.z *= pow(3, -d.y);
        else camZoom.y -= d.y*5;
    }
    if((lmb || rmb) && keyShift){
        vector<Bot>& bots = (lmb ? simulator.bots : simulator.destinations);
        while(simulator.bots.size() != simulator.destinations.size() && insertBot(bots, cursor));
    }
    return true;
}

void Scene::draw(){
    aspectRatio = (float)window.getSize().x/window.getSize().y;
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, window.getSize().x, window.getSize().y);
    gluPerspective(fov*180/M_PI, aspectRatio, 0.01, 100);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    vec3 eye = camPos+vec3(cos(camZoom.x)*cos(camZoom.y), sin(camZoom.y), -sin(camZoom.x)*cos(camZoom.y))*camZoom.z;
    vec3 up = vec3(cos(camZoom.x)*cos(camZoom.y+M_PI_2), sin(camZoom.y+M_PI_2), -sin(camZoom.x)*cos(camZoom.y+M_PI_2));
    gluLookAt(eye.x, eye.y, eye.z, camPos.x, camPos.y, camPos.z, up.x, up.y, up.z);

    Graphics::color(vec3(1, 1, 1));
    Graphics::grid(vec3(0, 0, 0), vec3(1, 0, 0), vec3(0, 0, 1), 5, 5);
    if(showCursor){
        Graphics::color(vec3(1, 1, 1));
        Graphics::sphere(cursor, Bot::r);
    }
    simulator.draw(showEndPoints, showPaths, showInterpolation);

    if(keyF2){
        sf::Texture t;
        t.create(window.getSize().x,window.getSize().y);
        t.update(window);
        stringstream ss;
        ss << simulator.timeSlice;
        string ts;
        ss >> ts;
        t.copyToImage().saveToFile("time_slice_" + ts + ".png");
    }

    window.display();
}

string Scene::getFilePath(bool load){
    OPENFILENAME ofn;
    char szFile[100];
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = 0;
    ofn.lpstrFile = szFile;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
    ofn.nFilterIndex = 1;
    ofn.lpstrFileTitle = 0;
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = 0;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;
    load ? GetOpenFileName(&ofn) : GetSaveFileName(&ofn);
    return ofn.lpstrFile;
}

void Scene::save(const string& path){
    ofstream file(path);
    file << simulator.bots.size() << endl;
    for(int i = 0; i < simulator.bots.size(); ++i){
        Bot& b = simulator.bots[i];
        file << b.pos << ' ' << b.ground << ' ' << b.connections.size() << ' ';
        for(int j = 0; j < b.connections.size(); ++j){
            file << b.connections[j] << ' ';
        }
        file << endl;
    }
    file << endl;
    file << endl;
    file << simulator.destinations.size() << endl;
    for(int i = 0; i < simulator.destinations.size(); ++i){
        Bot& b = simulator.destinations[i];
        file << b.pos << ' ' << b.ground << ' ' << b.connections.size() << ' ';
        for(int j = 0; j < b.connections.size(); ++j){
            file << b.connections[j] << ' ';
        }
        file << endl;
    }
}

void Scene::load(const string& path, std::vector<Bot>& bots){
    if(path.substr(path.size()-4, path.size()) == ".obj"){
        const int wantedNumber = 1000;
        ModelLoader ml;
        int a;
        int b = ceil(pow(wantedNumber, 1./3));
        vector<vec3> p;
        while(p.size() < wantedNumber){
            a = b;
            b *= pow(2, 1./3);
            p = ml.loadModel(path, b);
        }
        while(b-a > 1){
            int c = (a+b)/2;
            p = ml.loadModel(path, c);
            if(p.size() < wantedNumber) a = c;
            else b = c;
        }
        bots.clear();
        for(int i = 0; i < p.size(); ++i){
            bots.push_back(Bot(p[i]));
        }
        return;
    }

    simulator.reset();
    ifstream file(path);
    int n;
    file >> n;
    file.get();
    simulator.bots = vector<Bot>(n);
    for(int i = 0; i < n; ++i){
        Bot& b = simulator.bots[i];
        file >> b.pos >> b.ground;
        file.get();
        int c;
        file >> c;
        file.get();
        b.connections = vector<int>(c);
        for(int j = 0; j < b.connections.size(); ++j){
            file >> b.connections[j];
            file.get();
        }
        file.get();
    }
    file.get();
    file.get();
    file >> n;
    file.get();
    simulator.destinations = vector<Bot>(n);
    for(int i = 0; i < n; ++i){
        Bot& b = simulator.destinations[i];
        int c;
        file >> b.pos >> b.ground >> c;
        file.get();
        b.connections = vector<int>(c);
        for(int j = 0; j < b.connections.size(); ++j){
            file >> b.connections[j];
            file.get();
        }
        file.get();
    }
}

bool Scene::insertBot(std::vector<Bot>& bots, const vec3& pos){
    bots.push_back(Bot(pos));
    simulator.correct(bots);
    vec3 d = bots[bots.size()-1].pos-pos;
    float dist = sqrt(d*d);
    return dist > 2*Bot::r;
}

void Scene::insertBox(vector<Bot>& bots, const vec3& pos, const vec3& s, bool hollow){
    for(int i = 0; i < s.x; ++i){
        for(int j = 0; j < s.y; ++j){
            for(int k = 0; k < s.z; ++k){
                if(hollow && i > 0 && i < s.x-1 && j > 0 && j < s.y-1 && k > 0 && k < s.z-1) continue;
                bots.push_back(Bot(pos+vec3(i+0.5,j+0.5,k+0.5)));
            }
        }
    }
}
