#include "vec2.h"

#include <iostream>
#undef __STRICT_ANSI__
#include <math.h>
#include <stdlib.h>

using namespace std;

vec2::vec2(): x(0), y(0){}

vec2::vec2(double x, double y): x(x), y(y){}

vec2 operator-(const vec2& v){
    return vec2(-v.x,-v.y);
}

vec2 operator+(const vec2& v1, const vec2& v2){
    return vec2(v1.x+v2.x,v1.y+v2.y);
}

vec2 operator-(const vec2& v1, const vec2& v2){
    return vec2(v1.x-v2.x,v1.y-v2.y);
}

vec2 operator*(const vec2& v, double n){
    return vec2(v.x*n,v.y*n);
}

vec2 operator*(double n, const vec2& v){
    return v*n;
}

vec2 operator/(const vec2& v, double n){
    return v*(1/n);
}

void operator+=(vec2& v1, const vec2& v2){
    v1.x += v2.x;
    v1.y += v2.y;
}

void operator-=(vec2& v1, const vec2& v2){
    v1.x -= v2.x;
    v1.y -= v2.y;
}

void operator*=(vec2& v, double n){
    v.x *= n;
    v.y *= n;
}

void operator/=(vec2& v, double n){
    v *= 1/n;
}

double operator*(const vec2& v1, const vec2& v2){
    return v1.x*v2.x+v1.y*v2.y;
}

ostream& operator<<(ostream& os, const vec2& v){
    os << "{" << v.x << ", " << v.y << "}";
    return os;
}
