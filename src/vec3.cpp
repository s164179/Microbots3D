#include "vec3.h"

using namespace std;

vec3::vec3(): x(0), y(0), z(0){}

vec3::vec3(double x, double y, double z): x(x), y(y), z(z){}

bool operator==(const vec3& v1, const vec3& v2){
    return v1.x == v2.x && v1.z == v2.z;
}

bool operator!=(const vec3& v1, const vec3& v2){
    return !(v1 == v2);
}

vec3 operator-(const vec3& v){
    return vec3(-v.x,-v.y,-v.z);
}

vec3 operator+(const vec3& v1, const vec3& v2){
    return vec3(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);
}

vec3 operator-(const vec3& v1, const vec3& v2){
    return vec3(v1.x-v2.x,v1.y-v2.y,v1.z-v2.z);
}

vec3 operator*(const vec3& v, double n){
    return vec3(v.x*n,v.y*n,v.z*n);
}

vec3 operator*(double n, const vec3& v){
    return v*n;
}

vec3 operator/(const vec3& v, double n){
    return v*(1/n);
}

void operator+=(vec3& v1, const vec3& v2){
    v1.x += v2.x;
    v1.y += v2.y;
    v1.z += v2.z;
}

void operator-=(vec3& v1, const vec3& v2){
    v1.x -= v2.x;
    v1.y -= v2.y;
    v1.z -= v2.z;
}

void operator*=(vec3& v, double n){
    v.x *= n;
    v.y *= n;
    v.z *= n;
}

void operator/=(vec3& v, double n){
    v *= 1/n;
}

double operator*(const vec3& v1, const vec3& v2){
    return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;
}

ostream& operator<<(ostream& os, const vec3& v){
    os << "{" << v.x << ", " << v.y << ", " << v.z << "}";
    return os;
}

istream& operator>>(istream& is, vec3& v){
    is.get();
    is >> v.x;
    is.get();
    is >> v.y;
    is.get();
    is >> v.z;
    is.get();
    return is;
}
