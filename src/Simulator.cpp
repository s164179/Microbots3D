#include "Simulator.h"

#include <iostream>
#include <math.h>
#include <queue>
#define GLEW_STATIC
#include <GL/glew.h>
#define SFML_STATIC
#include <SFML/Graphics.hpp>

#include "Graphics.h"

using namespace std;

Simulator::Simulator(): timeSlice(0), dir(true){}

bool Simulator::update(){
    if(bots.size() != destinations.size()){
        cout << "Number of bots " << bots.size() << " is not number of destinations " << destinations.size() << endl;
        return false;
    }
    if(path.size() == 0 || path[0].size() != bots.size()){
        correct(bots);
        correct(destinations);
        path.clear();
        path.push_back(bots);
        path.push_back(destinations);
        assignmentProblem(path[0], path[1]);
    }
    vector<vector<Bot> > pathNew(path.size()*2-1);
    for(int i = 0; i < path.size(); ++i){
        pathNew[2*i] = path[i];
    }
    for(int i = 1; i < pathNew.size(); i += 2){
        pathNew[i] = interpolate(pathNew[max(i-3, 0)], pathNew[i-1], pathNew[i+1], pathNew[min(i+3, (int)pathNew.size()-1)]);
    }
    path = pathNew;
    cout << path.size() << endl;
    for(int iter = 0; iter < 100000/(path[0].size()*log(path[0].size())); ++iter){
        float maxDiff = 0;
        for(int i = 1; i < path.size()-1; ++i){
            vector<Bot> p = path[i];
            path[i] = interpolate(path[max(i-2, 0)], path[i-1], path[i+1], path[min(i+2, (int)path.size()-1)]);
            maxDiff = fmax(maxDiff, maximumDistance(p, path[i]));
        }
        for(int i = path.size()-2; i >= 1; --i){
            vector<Bot> p = path[i];
            path[i] = interpolate(path[max(i-2, 0)], path[i-1], path[i+1], path[min(i+2, (int)path.size()-1)]);
            maxDiff = fmax(maxDiff, maximumDistance(p, path[i]));
        }
        if(maxDiff < 0.001) break;
    }
    return true;
}

void Simulator::draw(bool showEndPoints, bool showPath, bool showInterpolation){
    if(!path.empty() && bots.size() == destinations.size()){
        if(showInterpolation){
            if(dir){
                ++timeSlice;
                if(timeSlice == path.size()-1) dir = !dir;
            }
            else{
                --timeSlice;
                if(timeSlice == 0) dir = !dir;
            }
            timeSlice = max(timeSlice, 0);
            timeSlice = min(timeSlice, (int)path.size()-1);
            Graphics::color(vec3(0, 0, 1));
            for(int i = 0; i < path[timeSlice].size(); ++i){
                Graphics::sphere(path[timeSlice][i].pos, Bot::r);
            }
        }
        if(showPath){
            glColor3f(1, 1, 1);
            for(int j = 0; j < path[0].size(); ++j){
                glBegin(GL_LINE_STRIP);
                for(int i = 0; i < path.size(); ++i){
                    vec3 p = path[i][j].pos;
                    glVertex3f(p.x, p.y, p.z);
                }
                glEnd();
            }
        }
    }
    if(!showEndPoints) return;
    Graphics::color(vec3(0, 1, 0));
    for(int i = 0; i < bots.size(); ++i){
        Graphics::sphere(bots[i].pos, Bot::r*0.5);
    }
    Graphics::color(vec3(1, 0, 0));
    for(int i = 0; i < destinations.size(); ++i){
        Graphics::sphere(destinations[i].pos, Bot::r*0.5);
    }
}

void Simulator::reset(){
    bots.clear();
    destinations.clear();
}

float Simulator::collision(vector<Bot>& bots){
    static sf::Clock cl;
    float timeStart = cl.getElapsedTime().asSeconds();

    struct Tree{
        int d = 0;
        Tree *a = 0, *b = 0;
        int id = -1;
        vec3 pos;
        ~Tree(){
            delete a;
            delete b;
        }
        void insert(int i, const vec3& p){
            if(id == -1){
                id = i;
                pos = p;
                a = new Tree();
                b = new Tree();
                a->d = b->d = (d+1)%3;
                return;
            }
            if(d == 0 && p.x < pos.x || d == 1 && p.y < pos.y || d == 2 && p.z < pos.z) a->insert(i,p);
            else b->insert(i,p);
        }
        void find(const vec3& p, float r, vector<int>& result){
            if(id == -1) return;
            if(d == 0 && p.x-r < pos.x || d == 1 && p.y-r < pos.y || d == 2 && p.z-r < pos.z) a->find(p, r, result);
            if(d == 0 && p.x+r > pos.x || d == 1 && p.y+r > pos.y || d == 2 && p.z+r > pos.z) b->find(p, r, result);
            vec3 d = p-pos;
            if(d*d < r*r) result.push_back(id);
        }
    };
    Tree tree;
    float maxDist = 0;
    for(int i = 0; i < bots.size(); ++i){
        if(bots[i].pos.y < Bot::r){
            maxDist = Bot::r-bots[i].pos.y;
            bots[i].pos.y = Bot::r;
        }
        vector<int> near;
        tree.find(bots[i].pos, Bot::r*2, near);
        for(int c = 0; c < near.size(); ++c){
            int j = near[c];
            vec3 d = bots[j].pos-bots[i].pos;
            float r = 2*Bot::r;
            while(d*d < 0.00000001){
                d += (vec3(rand(), rand(), rand())/RAND_MAX-vec3(0.5, 0.5, 0.5))*0.001;
            }
            if(d*d > r*r) continue;
            float dLen = sqrt(d*d);
            d /= dLen;
            dLen = dLen-r;
            d *= dLen;
            bots[i].pos += 0.5*d;
            bots[j].pos -= 0.5*d;
            maxDist = fmax(maxDist, fabs(dLen));
        }
        tree.insert(i, bots[i].pos);
    }

    static float t = 0;
    float dt = cl.getElapsedTime().asSeconds()-timeStart;
    t += dt;
    if(floor(t) > floor(t-dt)) cout << "collision: " << floor(t) << " seconds used" << endl;
    return maxDist;
}

float Simulator::resolveConnections(vector<Bot>& bots){
    static sf::Clock cl;
    float timeStart = cl.getElapsedTime().asSeconds();
    static int maxConnectionsStatic = 0;
    int maxConnections = 0;
    float maxDist = 0;
    for(int i = 0; i < bots.size(); ++i){
        maxConnections = max(maxConnections, (int)bots[i].connections.size());
        if(bots[i].ground) bots[i].pos.y = Bot::r;
        for(int c = 0; c < bots[i].connections.size(); ++c){
            int j = bots[i].connections[c];
            vec3 d = bots[j].pos-bots[i].pos;
            float dLen = sqrt(d*d);
            if(dLen < 2*Bot::r) continue;
            d /= dLen;
            dLen = dLen-2*Bot::r;
            d *= dLen;
            bots[i].pos += 0.5*d;
            bots[j].pos -= 0.5*d;
            maxDist = fmax(maxDist, fabs(dLen));
        }
    }
    if(maxConnections > maxConnectionsStatic){
        maxConnectionsStatic = maxConnections;
    }
    static float t = 0;
    float dt = cl.getElapsedTime().asSeconds()-timeStart;
    t += dt;
    if(floor(t) > floor(t-dt)) cout << "resolveConnections: " << floor(t) << " seconds used" << endl;
    return maxDist;
}

void Simulator::minimumTree(vector<Bot>& bots){
    static sf::Clock cl;
    float timeStart = cl.getElapsedTime().asSeconds();

    for(int i = 0; i < bots.size(); ++i){
        bots[i].ground = false;
        bots[i].connections.clear();
    }

    vector<bool> inTree(bots.size(), false);
    vector<float> dist(bots.size());
    for(int i = 0; i < dist.size(); ++i){
        dist[i] = bots[i].pos.y+Bot::r;
    }

    for(int i = 0; i < bots.size(); ++i){
        int jMin = -1, kMin = -1;
        float minDist;
        for(int j = 0; j < bots.size(); ++j){
            if(inTree[j]) continue;
            if(jMin == -1 || dist[j] < minDist){
                jMin = j;
                minDist = dist[j];
            }
        }
        minDist = bots[jMin].pos.y+Bot::r;
        for(int j = 0; j < bots.size(); ++j){
            if(!inTree[j]) continue;
            vec3 d = bots[jMin].pos-bots[j].pos;
            if(sqrt(d*d) < minDist){
                kMin = j;
                minDist = sqrt(d*d);
            }
        }
        if(kMin == -1){
            bots[jMin].ground = true;
        }
        else{
            bots[jMin].connections.push_back(kMin);
            bots[kMin].connections.push_back(jMin);
        }
        inTree[jMin] = true;
        for(int j = 0; j < bots.size(); ++j){
            if(inTree[j]) continue;
            vec3 d = bots[j].pos-bots[jMin].pos;
            dist[j] = fmin(dist[j], sqrt(d*d));
        }
    }

    static float t = 0;
    float dt = cl.getElapsedTime().asSeconds()-timeStart;
    t += dt;
    if(floor(t) > floor(t-dt)) cout << "minimumTree: " << floor(t) << " seconds used" << endl;
}

vector<Bot> Simulator::interpolate(const vector<Bot>& bots, const vector<Bot>& destinations, float w){
    vector<Bot> result(bots.size());
    for(int i = 0; i < bots.size(); ++i){
        result[i].pos = (1-w)*bots[i].pos+w*destinations[i].pos;
    }
    correct(result);
    return result;
}

vector<Bot> Simulator::interpolate(const vector<Bot>& b1, const vector<Bot>& b2, const vector<Bot>& b3, const vector<Bot>& b4){
    vector<Bot> result(b1.size());
    for(int i = 0; i < b1.size(); ++i){
        result[i].pos = (-b1[i].pos+4*b2[i].pos+4*b3[i].pos-b4[i].pos)/6;
        result[i].pos += (vec3(rand(), rand(), rand())/RAND_MAX-vec3(0.5, 0.5, 0.5))*0.00001;
    }
    correct(result);
    return result;
}

void Simulator::correct(vector<Bot>& bots){
    minimumTree(bots);
    float maxDist = 1;
    while(maxDist > 0.1*Bot::r){
        maxDist = fmax(collision(bots), resolveConnections(bots));
        if(maxDist > Bot::r) minimumTree(bots);
    }
}

float Simulator::maximumDistance(const vector<Bot>& bots, const vector<Bot>& destinations){
    float result = 0;
    for(int i = 0; i < bots.size(); ++i){
        vec3 d = destinations[i].pos-bots[i].pos;
        float dist = sqrt(d*d);
        result = fmax(result, dist);
    }
    return result;
}

float Simulator::squareSumDistance(const vector<Bot>& bots, const vector<Bot>& destinations){
    float result = 0;
    for(int i = 0; i < bots.size(); ++i){
        vec3 d = destinations[i].pos-bots[i].pos;
        result += d*d;
    }
    return result;
}

float Simulator::squareSumAcceleration(const vector<Bot>& b1, const vector<Bot>& b2, const vector<Bot>& b3){
    float result = 0;
    for(int i = 0; i < bots.size(); ++i){
        vec3 d1 = b2[i].pos-b1[i].pos;
        vec3 d2 = b3[i].pos-b2[i].pos;
        vec3 a = d2-d1;
        result += a*a;
    }
    return result;
}

void Simulator::assignmentProblem(const std::vector<Bot>& bots, std::vector<Bot>& destinations){
    if(bots.empty()) return;
    cout << "Solving assignment problem" << endl;
    bool change = true;
    while(change){
        change = false;
        for(int i = 0; i < bots.size(); ++i){
            for(int c = 0; c < bots[i].connections.size(); ++c){
                int j = bots[i].connections[c];
                vec3 dii = destinations[i].pos-bots[i].pos;
                vec3 dij = destinations[j].pos-bots[i].pos;
                vec3 dji = destinations[i].pos-bots[j].pos;
                vec3 djj = destinations[j].pos-bots[j].pos;
                if(dii*dii+djj*djj > dij*dij+dji*dji+0.0001){
                    swap(destinations[i], destinations[j]);
                    change = true;
                }
            }
        }
        for(int iter = 0; iter < 1000; ++iter){
            int i = rand()%bots.size();
            int j = rand()%bots.size();
            if(i == j) continue;
            vec3 dii = destinations[i].pos-bots[i].pos;
            vec3 dij = destinations[j].pos-bots[i].pos;
            vec3 dji = destinations[i].pos-bots[j].pos;
            vec3 djj = destinations[j].pos-bots[j].pos;
            if(dii*dii+djj*djj > dij*dij+dji*dji+0.0001){
                swap(destinations[i], destinations[j]);
                change = true;
            }
        }
    }
    destinations = interpolate(destinations, destinations, 0);
    cout << "Assignment problem solved" << endl;
}
